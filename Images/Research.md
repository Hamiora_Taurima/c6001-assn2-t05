# Research Report Milestone One


As a group we decided we want to create a response bot that replied to a user input. The main focus being a genuine reply to the user input for example if the user asked "how was your day" the response being generated would be in line with an appropriate answer.

We discussed possible api's to fulfill this role such as the [Wit](https://wit.ai/) which is exactly in line with the idea of the app we wanted to create.  To add another level to our application we started looking into having a video search function like [Youtube](https://developers.google.com/youtube/iframe_api_reference) so the user could listen to music while interacting with the app, another possible function we thought to add was a google search function that allowed the user to browse while having the app open  

The next step we moved into three groups of three and proceeded to develop our wireframes. We split into 3 groups of two and developed our page layout ideas we also decided to add a chatlog save feature and a login page.

## Wireframes
<img src="Appmain wireframe.png" alt="" align="centre">
<img src="Chatlog wireframe.png" alt="" align="centre">
<img src="Desktop view.png" alt="" align="centre">
<img src="Log in Wireframe.png" alt="" align="centre">

## Tools

The tools we will focus on using for milestone 2 will be Discord, slack and google drive. 

Discord is a stable easy to use application that allows voip seeing as it cant be quite difficult to explain an issue over written messages discord allows us to bridge this gap easily as it also has a web browser page that can connect to voice if you cant download the app.

Google drive is our main source of pulling documentation together because it has an easy to use function for teams to share any documentation securly.

